//es5
//Constructor
function Book(title, author, year){
    this.title = title;
    this.author = author;
    this.year = year;
}

//getSummary prototype
//supaya tidak disimpan di object
Book.prototype.getSummary = function(){
    return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
}

//get book aget
Book.prototype.getAge = function(){
    const years = new Date().getFullYear - this.year
    return `Judul ${this.title} telah berumur ${years} tahun`
}

//Revise perubahan tahun
Book.prototype.revise = function(newYear){
    this.year = newYear;
    this.revised = true;
}

//instantiate
const book1 = new Book('Book one', 'Rudi', '2012');
const book2 = new Book('Book two', 'Rida', '2014');

console.log(book1.getSummary())
// console.log(book2)
// book2.revise('2018')
// console.log(book2)