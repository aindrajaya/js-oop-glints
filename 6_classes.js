class Book{
    constructor(title, author, year){
        this.title = title
        this.author = author
        this.year = year
    }

    //add method get summary
    getSummary(){
        return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
    }

    //add method get year
    getAge(){
        const years = new Date().getFullYear - this.year
        return `Judul ${this.title} telah berumur ${years} tahun`
    }

    //revise year book
    revise(newYear){
        this.year = newYear
        this.revised = true
    }

    static topBookStore(){
        return 'Gramedia';
    }
}

///instantiate
const book1 = new Book('Book satu', 'Rudi', '2012');

// console.log(book1)
// book1.revised('2017')
// console.log(book1)

console.log(book1);