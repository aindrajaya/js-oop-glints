class Human{
    static isLivingOnEarth = true;
    static group = "Vertebrate";
    constructor(name, isAlive){
        this.name = name;
        this.isAlive = isAlive;
    }

    introduce(){
        console.log(`Hello, My name is ${this.name} and I'm ${this.isAlive ? "Alive" : "dead"}`);
    }

}

//add prototype/instance method
Human.prototype.greet = function(name){
    console.log(`Hi, ${name}, I'm ${this.name}`)
}

Human.destroy = function(thing){
    console.log(`Human is destroying ${thing}`)
}

let mj = new Human("Michael Jackson");
let rd = new Human("Rudi");

rd.greet("Michael");

mj.introduce()
rd.introduce()