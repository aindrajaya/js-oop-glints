class User {
    constructor(props) {
      // props is object, because it is better that way
      let { email, password } = props; // Destruct
      this.email = email;
      this.encryptedPassword = this._encrypt(password); // We won't save the plain password
    }
  
    // Private method
    _encrypt = (password) => {
      return `pretend-this-is-an-encrypted-version-of-${password}`
    }
  
    // Getter
    _decrypt = () => {
      return this.encryptedPassword.split(`pretend-this-is-an-encrypted-version-of-`)[1];
    }
  
    authenticate(password) {
      return this._decrypt() === password; // Will return true or false
    }
  }
  
  //instantiate
  let Isyana = new User({
    email: "IsyanaKarina@mail.com",
    password: "123456"
  })
  
  const isAuthenticated = Isyana.authenticate("12347756");
  console.log(isAuthenticated) // true
  
  