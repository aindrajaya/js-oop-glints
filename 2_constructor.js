//es5
//Constructor
function Book(title, author, year){
    this.title = title;
    this.author = author;
    this.year = year;

    this.getSummary = function(){
        return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
    }
}

//instantiate
const book1 = new Book('Book one', 'Rudi', '2020');
const book2 = new Book('Book two', 'Rida', '2019');

console.log(book2.getSummary())