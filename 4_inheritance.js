//es5
//Constructor
function Book(title, author, year){
    this.title = title;
    this.author = author;
    this.year = year;
}

//getSummary prototype
//supaya tidak disimpan di object
Book.prototype.getSummary = function(){
    return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
}

//Constructor majalah
function Majalah(title, author, year, month){
    Book.call(this, title, author, year);
    this.month = month;
}

//inheritance prototype
Majalah.prototype = Object.create(Book.prototype);

//instantiate object majalah
const majalah1 = new Majalah('Mag One', 'Rudi', '2012', 'Jan')
const book1 = new Book('Mag One', 'Rudi', '2012')

//set magazzine constructor
Majalah.prototype.constructor = Majalah

console.log(majalah1)
console.log(book1)
