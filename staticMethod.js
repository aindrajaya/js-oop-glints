class Human{
    static isLivingOnEarth = true;
    static group = "Vertebrate";
    constructor(name, isAlive){
        this.name = name;
        this.isAlive = isAlive;
    }

    introduce(){
        console.log(`Hello, My name is${this.name} and I'm ${this.isAlive ? "Alive" : "dead"}`);
    }

    static isEating(food){
        let foods = ["plant", "animal"];
        return foods.includes(food.toLowerCase());
    }
}

console.log(Human.isEating("PLANT"));
console.log(Human.isEating("Human"));