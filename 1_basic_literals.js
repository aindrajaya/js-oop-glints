const buku1 = {
    title: 'Book One',
    author: 'Rudi',
    year: '2020',
    getSummary : function(){
        return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
    }
}

const buku2 = {
    title: 'Book Two',
    author: 'Rida',
    year: '2019',
    getSummary : function(){
        return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
    }
}

console.log(buku1)
console.log(buku2)