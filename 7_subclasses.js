class Book{
    constructor(title, author, year){
        this.title = title
        this.author = author
        this.year = year
    }

    //add method get summary
    getSummary(){
        return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
    }
}

//sub class majalah
class Majalah extends Book(){
    constructor(title, author, year, month){
        super(title, author, year);
        this.month = month
    }
}

//instantiate majalah
const majalah1 = new Majalah('Majalah Satu', 'Rudi', '2012', 'Feb')

console.log(majalah1)
console.log(majalah1.getSummary())