class Human{
    constructor(name, address){
        this.name = name;
        this.address = address;
    }

    introduce(){
        console.log(`Hi, my name ${this.name}`)
    }

    work(){
        console.log("Work!!")
    }
}

class Programmer extends Human{
    constructor(name, address, programmingLanguages){
        super(name, address);
        this.programmingLanguages = programmingLanguages;
    }

    introduce(){
        super.introduce()
        console.log(`dan saya dapat menggunakan bahasa pemrograman`, this.programmingLanguages);
    }

    code(){
        console.log(
            "Code Some",
            this.programmingLanguages[
                Math.floor(Math.random() * this.programmingLanguages.length)
            ]
        )
    }
}

//instantiate
let pro1 = new Programmer("Rudi", "Jogja", "Javascript");

console.log(pro1.introduce());
console.log(pro1.code());
