//object dari prototype
const bookProtos = {
    getSummary: function(){
        return `${this.title} ditulis oleh ${this.author} pada tahun ${this.year}`
    },

    getAge: function(){
        const years = new Date().getFullYear - this.year
        return `Judul ${this.title} telah berumur ${years} tahun`
    }
}

//create object
// const book1 = Object.create(bookProtos);
// book1.title = 'Book Satu'
// book1.author = 'Rudi'
// book1.year = '2013'

//or you can use
const book1 = Object.create(bookProtos, {
    title: {value: 'Book Satu'},
    author: {value: 'Rudi'},
    year: {value: '2013'},
})

console.log(book1);