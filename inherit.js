class Human{
    constructor(name, address){
        this.name = name;
        this.address = address;
    }

    introduce(){
        console.log(`Hi, my name ${this.name}`)
    }

    work(){
        console.log("Work!!")
    }
}
